use crate::Solution;

impl Solution {
    pub fn remove_element(nums: &mut Vec<i32>, val: i32) -> i32 {
        let mut count = 0;
        for (i, e) in nums.clone().iter().enumerate() {
            if e == &val {
                count += 1;
            } else {
                nums[i - count] = *e;
            }
        }
        (nums.len() - count) as i32
    }
}

pub fn run() {
    let mut nums = vec![1,2,4,5,3,5,4,2,4,5,4,1,1];
    Solution::remove_element(&mut nums, 4);
}
