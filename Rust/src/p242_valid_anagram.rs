use crate::Solution;

impl Solution {
    pub fn is_anagram(s: String, t: String) -> bool {
        let mut letter_map = std::collections::BTreeMap::new();
        s.chars().for_each(|c| *letter_map.entry(c).or_insert(0) += 1);
        t.chars().for_each(|c| *letter_map.entry(c).or_insert(0) -= 1);
        !letter_map.into_values().any(|v| v != 0)
    }
}

pub fn run() {
    let s = "anagram".to_string();
    let t = "nagaram".to_string();
    assert!(Solution::is_anagram(s, t));

    let s = "rat".to_string();
    let t = "car".to_string();
    assert!(!Solution::is_anagram(s, t));
}
