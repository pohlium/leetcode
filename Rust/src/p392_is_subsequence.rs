use crate::Solution;

impl Solution{
    /// Test whether s is a subsequence of t
    pub fn is_subsequence(s: String, t: String) -> bool {
        let mut t_iter = t.chars();
        for char_s in s.chars() {
            let mut found_match = false;
            for char_t in t_iter.by_ref() {
                if char_t == char_s {
                    found_match = true;
                    break;
                }
            }
            if !found_match {
                return false;
            }
        }
        true
    }
}

pub fn run() {
    let s = "abc".to_string();
    let t = "ahbgdc".to_string();
    assert!(Solution::is_subsequence(s, t));

    let s = "axc".to_string();
    let t = "ahbgdc".to_string();
    assert!(!Solution::is_subsequence(s, t));
}
