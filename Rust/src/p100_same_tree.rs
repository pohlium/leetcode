use crate::Solution;

use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

impl Solution {
    fn recursive_traverse(root: &Option<Rc<RefCell<TreeNode>>>, nodes: &mut Vec<Option<i32>>) {
        match root {
            Some(root) => {
                nodes.push(Some(root.borrow().val));
                Solution::recursive_traverse(&root.borrow().left, nodes);
                Solution::recursive_traverse(&root.borrow().right, nodes);
            }
            None => {
                nodes.push(None);
            }
        };
    }

    pub fn is_same_tree(
        p: Option<Rc<RefCell<TreeNode>>>,
        q: Option<Rc<RefCell<TreeNode>>>,
    ) -> bool {
        let mut p_nodes = Vec::new();
        let mut q_nodes = Vec::new();
        Solution::recursive_traverse(&p, &mut p_nodes);
        Solution::recursive_traverse(&q, &mut q_nodes);
        p_nodes == q_nodes
    }
}

pub fn run() {
    let p_rleaf = Some(Rc::new(RefCell::new(TreeNode::new(3))));
    let p_lleaf = Some(Rc::new(RefCell::new(TreeNode::new(2))));
    let p = Some(Rc::new(RefCell::new(TreeNode::new(1))));
    p.clone().unwrap().borrow_mut().right = p_rleaf;
    p.clone().unwrap().borrow_mut().left = p_lleaf;

    let q_rleaf = Some(Rc::new(RefCell::new(TreeNode::new(3))));
    let q_lleaf = Some(Rc::new(RefCell::new(TreeNode::new(2))));
    let q = Some(Rc::new(RefCell::new(TreeNode::new(1))));
    q.clone().unwrap().borrow_mut().right = q_rleaf;
    q.clone().unwrap().borrow_mut().left = q_lleaf;

    assert!(Solution::is_same_tree(p, q));

    let p_rleaf = None;
    let p_lleaf = Some(Rc::new(RefCell::new(TreeNode::new(2))));
    let p = Some(Rc::new(RefCell::new(TreeNode::new(1))));
    p.clone().unwrap().borrow_mut().right = p_rleaf;
    p.clone().unwrap().borrow_mut().left = p_lleaf;

    let q_rleaf = Some(Rc::new(RefCell::new(TreeNode::new(3))));
    let q_lleaf = None;
    let q = Some(Rc::new(RefCell::new(TreeNode::new(1))));
    q.clone().unwrap().borrow_mut().right = q_rleaf;
    q.clone().unwrap().borrow_mut().left = q_lleaf;

    assert!(!Solution::is_same_tree(p, q));
}
