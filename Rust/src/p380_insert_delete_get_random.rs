struct RandomizedSet {
    map: std::collections::HashMap<i32, usize>,
    vec: Vec<i32>,
}

impl RandomizedSet {
    fn new() -> Self {
        Self {
            map: std::collections::HashMap::new(),
            vec: Vec::new(),
        }
    }

    fn insert(&mut self, val: i32) -> bool {
        if self.map.contains_key(&val) {
            return false;
        }
        self.map.insert(val, self.vec.len());
        self.vec.push(val);
        true
    }

    fn remove(&mut self, val: i32) -> bool {
        if let Some(v_idx) = self.map.remove(&val) {
            self.vec.swap_remove(v_idx);
            // Since we potentially moved the last element to v_idx, we now have to correct the
            // index in our map
            if let Some(&elem) = self.vec.get(v_idx) {
                self.map.insert(elem, v_idx);
            }
            true
        } else {
            false
        }
    }

    fn get_random(&self) -> i32 {
        use rand::seq::SliceRandom;
        *self.vec.choose(&mut rand::thread_rng()).unwrap()
    }
}

pub fn run() {
    let mut r = RandomizedSet::new();
    assert!(r.insert(1));
    assert!(!r.remove(2));
    assert!(r.insert(2));
    r.get_random();
    assert!(r.remove(1));
    assert!(!r.insert(2));
    r.get_random();
}
