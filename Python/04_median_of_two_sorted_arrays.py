import statistics
import math

MAX = 10**7
MIN = -10**7

def findMedianSortedArrays(nums1: list[int], nums2: list[int]) -> float:
    if len(nums1) == 0:
        return statistics.median(nums2)
    elif len(nums2) == 0:
        return statistics.median(nums1)

    target_idx = math.floor((len(nums1) + len(nums2)) / 2)
    have_to_avg = (len(nums1) + len(nums2)) % 2 == 0

    NUMS1_ID = 0
    NUMS2_ID = ~NUMS1_ID

    lists = { NUMS1_ID: nums1, NUMS2_ID: nums2 }
    idxs = {}


    cur_list_id = min((NUMS1_ID, NUMS2_ID), key=lambda e: lists[e][0])
    idxs[cur_list_id] = 0
    idxs[~cur_list_id] = -1

    prev_list_id = None
    prev_idx = 0

    for _ in range(target_idx):
        next_i = nums1[idxs[NUMS1_ID] + 1] if len(nums1) > idxs[NUMS1_ID] + 1 else MAX
        next_j = nums2[idxs[NUMS2_ID] + 1] if len(nums2) > idxs[NUMS2_ID] + 1 else MAX

        prev_list_id = cur_list_id
        prev_idx = idxs[cur_list_id]
        if next_i < next_j:
            idxs[NUMS1_ID] += 1
            cur_list_id = NUMS1_ID
        else:
            idxs[NUMS2_ID] += 1
            cur_list_id = NUMS2_ID

    if have_to_avg:
        return (lists[cur_list_id][idxs[cur_list_id]] + lists[prev_list_id][prev_idx]) / 2
    else:
        return lists[cur_list_id][idxs[cur_list_id]]


if __name__ == '__main__':
    a = [0,5,8,33]
    b = [2,4,5,7,8,14]
    print(findMedianSortedArrays(a, b))
