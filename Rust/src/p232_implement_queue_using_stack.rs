// p232 implement a FIFO queue, using only the language's std lib `stack` (LIFO) or a more powerful
// datastructure but only its stack operations.
// -> Vec, Vec::push, Vec::peek, Vec::pop, Vec::empty
struct MyQueue {
    data: Vec<i32>,
    swap: Vec<i32>,
}

// Better approach: instead of swapping the values in and out on every push, you could have one vec
// acting as a stack (so the normal vector direction) and one acting as a queue (with the element
// order swapped).
// You always push to the stack (so it doesn't cost anything extra).
// When popping or peeking, you check whether the queue is empty.
// If the queue is not empty, you just pop from the queue (since the values there are / will be
// ordered the other way around, so popping gets you the correct value).
// If the queue is empty, only then do we once flip all the values from the stack to the queue (so
// our "swap-in" i guess) and then we can work on the values of the queue again as long as it has
// values to work on.
impl MyQueue {
    fn new() -> Self {
        Self {
            data: Vec::new(),
            swap: Vec::new(),
        }
    }

    fn swap_out(&mut self) {
        while let Some(e) = self.data.pop() {
            self.swap.push(e)
        }
    }

    fn swap_in(&mut self) {
        while let Some(e) = self.swap.pop() {
            self.data.push(e)
        }
    }
    fn push(&mut self, x: i32) {
        self.swap_out();
        self.data.push(x);
        self.swap_in()
    }

    fn pop(&mut self) -> i32 {
        self.data.pop().unwrap()
    }

    fn peek(&mut self) -> i32 {
        *self.data.last().unwrap()
    }

    fn empty(&self) -> bool {
        self.data.is_empty()
    }
}

pub fn run() {
    let mut q = MyQueue::new();
    q.push(1);
    q.push(2);
    println!("Peek: {}", q.peek());
    println!("pop: {}", q.pop());
    println!("empty: {}", q.empty());
    println!("pop: {}", q.pop());
    println!("empty: {}", q.empty());

}
