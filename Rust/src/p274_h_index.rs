use crate::Solution;

impl Solution {
    pub fn h_index(citations: Vec<i32>) -> i32 {
        let mut citations = citations;
        citations.sort_unstable();
        // Special case: all papers have no citations
        if *citations.last().unwrap() == 0 {
            return 0;
        }
        let clen = citations.len() as i32;
        // We iterate through papers starting with the most cited one, until the number of papers
        // we have already counted is larger than the number of citations of the paper.
        for (i, e) in citations.into_iter().rev().enumerate() {
            let no_of_papers = (i + 1) as i32;
            if no_of_papers > e {
                return no_of_papers - 1;
            }
        }
        // if that case never happens, then all our papers have more citations than the total
        // number of papers we have published, so we just return the number of papers we published
        // as our h index
        clen
    }
}

pub fn run() {
    let citations = vec![3, 0, 6, 1, 5];
    assert_eq!(Solution::h_index(citations), 3);

    let citations = vec![100];
    assert_eq!(Solution::h_index(citations), 1);

    let citations = vec![1, 3, 1];
    assert_eq!(Solution::h_index(citations), 1);
}
