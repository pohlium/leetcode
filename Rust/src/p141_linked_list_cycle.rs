use std::cell::RefCell;
use std::rc::Rc;

use crate::Solution;

#[derive(Default, PartialEq)]
pub struct ListNode {
    val: i32,
    next: Option<Rc<RefCell<Self>>>,
}

impl ListNode {
    pub fn new(val: i32, next: Option<Rc<RefCell<Self>>>) -> Self {
        ListNode { val, next }
    }
}

impl Solution {
    pub fn has_cycle(head: ListNode) -> bool {
        // floyds cycle finding algo
        let mut fast = head.next.clone();
        let mut slow = Some(Rc::new(RefCell::new(head)));
        while slow.is_some() && fast.is_some() {
            if *slow.clone().unwrap().borrow() == *fast.clone().unwrap().borrow() {
                return true;
            }
            slow = slow.unwrap().borrow().next.clone();
            fast = fast
                .unwrap()
                .borrow()
                .next
                .clone()
                .unwrap_or(Rc::new(RefCell::new(ListNode::default())))
                .borrow()
                .next
                .clone();
        }
        todo!()
    }
}

pub fn run() {
    let tail = Rc::new(RefCell::new(ListNode::new(-4, None)));
    let t1 = Rc::new(RefCell::new(ListNode::new(0, Some(tail.clone()))));
    let t2 = Rc::new(RefCell::new(ListNode::new(2, Some(t1))));
    let head = ListNode::new(3, Some(t2.clone()));
    tail.borrow_mut().next = Some(t2.clone());
    assert!(Solution::has_cycle(head));
}
