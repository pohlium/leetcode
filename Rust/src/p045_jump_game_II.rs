use crate::Solution;

impl Solution {
    pub fn jump(nums: Vec<i32>) -> i32 {
        let mut jumps = 0;
        let mut max_jump = 0;
        let mut current_pos = 0;
        let target = nums.len() - 1;

        for (i, num) in nums.into_iter().enumerate() {
            if current_pos >= target {
                return jumps;
            }
            max_jump = max_jump.max(num as usize + i);
            if current_pos == i {
                jumps += 1;
                current_pos = max_jump;
            }
        }
        // We early return inside the loop as soon as we have reached the end of the array, because
        // otherwise the last element sometimes adds one to our jump, which is bad.
        // However, this means that we have to panic if we don't actually return to satisfy rust
        panic!()
    }
}

pub fn run() {
    let nums = vec![2,3,1,1,4];
    assert_eq!(Solution::jump(nums), 2);

    let nums = vec![2,3,0,1,4];
    assert_eq!(Solution::jump(nums), 2);
}
