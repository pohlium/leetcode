use crate::Solution;

impl Solution {
    pub fn str_str(haystack: String, needle: String) -> i32 {
        haystack.find(&needle).map(|i| i as i32).unwrap_or(-1)
    }
}

pub fn run() {
    let haystack = "sadbutsad".to_owned();
    let needle = "sad".to_owned();
    assert_eq!(Solution::str_str(haystack, needle), 0);

    let haystack = "leetcode".to_owned();
    let needle = "leeto".to_owned();
    assert_eq!(Solution::str_str(haystack, needle), -1);
}
