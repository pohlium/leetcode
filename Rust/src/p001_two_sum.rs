use crate::Solution;

impl Solution {
    pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
        let mut seen = std::collections::HashMap::new();
        for (i, val) in (0i32..).zip(nums.iter()) {
            if let Some(idx) = seen.get(&(target - val)) {
                return vec![i, *idx];
            }
            seen.insert(*val, i);
        }
        vec![-1, -1]
    }
}

pub fn run() {
    Solution::two_sum(vec![1, 2, 3, 4, 5, 6, 7, 8], 15);
}
