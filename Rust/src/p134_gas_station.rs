use crate::Solution;

impl Solution {
    pub fn can_complete_circuit(gas: Vec<i32>, cost: Vec<i32>) -> i32 {
        let mut min_gas = 0;
        let mut min_idx = 0;
        let mut current_gas = 0;
        for (i, i_cost) in gas
            .into_iter()
            .zip(cost.into_iter())
            .map(|(g, c)| g - c)
            .enumerate()
        {
            current_gas += i_cost;
            if current_gas < min_gas {
                min_gas = current_gas;
                min_idx = i + 1;
            }
        }
        if current_gas >= 0 {
            min_idx as i32
        } else {
            -1
        }
    }
}

pub fn run() {
    let (gas, cost) = (vec![1, 2, 3, 4, 5], vec![3, 4, 5, 1, 2]);
    assert_eq!(Solution::can_complete_circuit(gas, cost), 3);

    let (gas, cost) = (vec![2, 3, 4], vec![3, 4, 3]);
    assert_eq!(Solution::can_complete_circuit(gas, cost), -1);
}
