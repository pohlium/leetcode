use crate::Solution;

impl Solution {
    pub fn longest_common_prefix(strs: Vec<String>) -> String {
        #[inline]
        fn longest_prefix<'a>(a: &'a str, b: &'a str) -> &'a str {
            for (i, (c1, c2)) in a.chars().zip(b.chars()).enumerate() {
                if c1 != c2 {
                    return &a[..i];
                }
            }
            // In case all characters matched, return the shorter word
            if a.len() > b.len() {
                b
            } else {
                a
            }
        }

        // by constraing we know that strs.len() >= 1
        let mut currently_longest_prefix = strs[0].as_str();
        for word in strs.iter() {
            currently_longest_prefix = longest_prefix(currently_longest_prefix, word);
        }
        currently_longest_prefix.to_string()
    }
}

pub fn run() {
    let strs = ["flower", "flow", "flight"]
        .iter()
        .map(|s| s.to_string())
        .collect();
    assert_eq!(Solution::longest_common_prefix(strs), "fl".to_string());

    let strs = ["dog", "racecar", "car"]
        .iter()
        .map(|s| s.to_string())
        .collect();
    assert_eq!(Solution::longest_common_prefix(strs), "".to_string());

    let strs = ["ab", "a"].iter().map(|s| s.to_string()).collect();
    assert_eq!(Solution::longest_common_prefix(strs), "a".to_string());
}
