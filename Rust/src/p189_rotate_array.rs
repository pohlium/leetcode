use crate::Solution;

impl Solution {
    pub fn rotate(nums: &mut Vec<i32>, k: i32) {
        let vec_len = nums.len();
        let k = k as usize % vec_len;
        // built-in solution:
        // nums.rotate_right(k); // this needs k < nums.len()
        let no_tmp_values = vec_len - k;
        let tmp_values: Vec<i32> = nums[..no_tmp_values].to_vec();
        for i in no_tmp_values..vec_len {
            nums[i - no_tmp_values] = nums[i];
        }
        for i in (vec_len - no_tmp_values)..vec_len {
            nums[i] = tmp_values[i - (vec_len - no_tmp_values)];
        }
    }
}

pub fn run() {
    let mut nums = vec![1, 2, 3, 4, 5, 6, 7];
    Solution::rotate(&mut nums, 3);
    assert_eq!(nums, vec![5, 6, 7, 1, 2, 3, 4]);

    let mut nums = vec![-1, -100, 3, 99];
    Solution::rotate(&mut nums, 2);
    assert_eq!(nums, vec![3, 99, -1, -100]);
}
