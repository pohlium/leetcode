pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        Self { next: None, val }
    }

    fn print(&self) {
        let mut sum = self.val;
        let mut mult = 10;
        let mut ptr = &self.next;
        while ptr.is_some() {
            sum += mult * ptr.as_ref().unwrap().val;
            mult *= 10;
            ptr = &ptr.as_ref().unwrap().next;
        }
        println!("{}", sum);
    }
}

impl crate::Solution {
    fn add_two_numbers(
        mut l1: Option<Box<ListNode>>,
        mut l2: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        let zero_node = Box::new(ListNode::new(0));
        let mut sum = l1.as_ref().unwrap_or(&zero_node).val + l2.as_ref().unwrap_or(&zero_node).val;
        let mut carry = sum / 10;
        sum %= 10;
        let mut result = Some(Box::new(ListNode::new(sum)));
        let mut ptr = &mut result;

        l1 = l1.unwrap().next;
        l2 = l2.unwrap().next;
        while l1.is_some() || l2.is_some() {
            let v1 = l1.as_ref().unwrap_or(&zero_node).val;
            let v2 = l2.as_ref().unwrap_or(&zero_node).val;
            println!("Adding {v1} and {v2} + carry {carry}");
            sum = v1 + v2 + carry;
            println!("Presum: {sum}");
            carry = sum / 10;
            println!("Carry: {carry}");
            sum %= 10;
            println!("Sum: {sum}");
            ptr.as_mut().unwrap().next = Some(Box::new(ListNode::new(sum)));
            ptr = &mut ptr.as_mut().unwrap().next;
            if l1.is_some() {
                l1 = l1.unwrap().next;
            }
            if l2.is_some() {
                l2 = l2.unwrap().next;
            }
        }
        if carry > 0 {
            println!("Adding final node with value {carry}");
            ptr.as_mut().unwrap().next = Some(Box::new(ListNode::new(carry)));
        }
        result
    }
}

fn make_node(val: i32) -> Option<Box<ListNode>> {
    Some(Box::new(ListNode::new(val)))
}

pub fn run() {
    let mut a = make_node(9);
    let mut b = make_node(9);
    let c = make_node(9);

    let mut d = make_node(9);
    let e = make_node(9);
    // let f = make_node(9);

    b.as_mut().unwrap().next = c;
    a.as_mut().unwrap().next = b;

    // e.as_mut().unwrap().next = f;
    d.as_mut().unwrap().next = e;

    a.as_ref().unwrap().print();
    d.as_ref().unwrap().print();
    let res = crate::Solution::add_two_numbers(a, d);
    res.unwrap().print();
}
