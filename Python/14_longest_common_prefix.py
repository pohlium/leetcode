class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
      if len(strs) == 0:
        return ""
      # find the shortest word
      shortest_word = strs[0]
      shortest_len = len(shortest_word)
      for word in strs:
        wordlen = len(word)
        if wordlen < shortest_len:
          shortest_word = word
          shortest_len = wordlen
      # iterate through substrings x[0:len(x)] - x[len(x):len(x)]
      longest_common_prefix = ""
      for x in range(1, shortest_len + 1):
        substr = shortest_word[:x]
        for word in strs:
          if not word.startswith(substr):
            break
        else:
          longest_common_prefix = substr
          continue
        break
      return longest_common_prefix
