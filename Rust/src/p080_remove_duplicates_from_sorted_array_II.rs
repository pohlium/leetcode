use crate::Solution;

impl Solution {
    /// IN: sorted array in non-decreasing order
    /// TASK: remove duplicates such that each unique element appears at most twice.
    ///       this has to be done in-place in the input array
    /// OUT: Number of elements that remain after the purge
    pub fn remove_duplicates_2(nums: &mut Vec<i32>) -> i32 {
        let mut write_ptr = 0;
        let mut count = 0;
        let mut triplicates_count = 0;
        let mut current_num = nums[0];
        for i in 0..nums.len() {
            if nums[i] != current_num {
                current_num = nums[i];
                count = 1;
            } else {
                count += 1;
            }

            if count > 2 {
                write_ptr -= 1;
                triplicates_count += 1;
            }
            nums[write_ptr] = nums[i];
            write_ptr += 1;
        }
        nums.len() as i32 - triplicates_count
    }
}

pub fn run() {
    let mut nums = vec![1, 1, 1, 2, 2, 3];
    let k = Solution::remove_duplicates_2(&mut nums);
    assert_eq!(&nums[..(k as usize)], [1, 1, 2, 2, 3]);

    let mut nums = vec![0, 0, 1, 1, 1, 1, 2, 3, 3];
    let k = Solution::remove_duplicates_2(&mut nums);
    assert_eq!(&nums[..(k as usize)], [0, 0, 1, 1, 2, 3, 3]);
}
