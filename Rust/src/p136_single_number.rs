use crate::Solution;

impl Solution {
    pub fn single_number(nums: Vec<i32>) -> i32 {
        todo!()
    }
}

pub fn run() {
    let nums = vec![2,2,1];
    assert_eq!(Solution::single_number(nums), 1);

    let nums = vec![4,1,2,1,2];
    assert_eq!(Solution::single_number(nums), 4);

    let nums = vec![1];
    assert_eq!(Solution::single_number(nums), 1);
}
