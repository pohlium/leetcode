use crate::Solution;

impl Solution {
    fn search_insert_builtin(nums: Vec<i32>, target: i32) -> i32 {
        match nums.binary_search(&target) {
            Ok(i) => i as i32,
            Err(i) => i as i32
        }
    }

    pub fn search_insert(nums: Vec<i32>, target: i32) -> i32 {
        use std::cmp::Ordering;
        let mut min = 0;
        let mut max = nums.len() as i32 - 1;
        while min <= max {
            println!("min: {min}, max: {max}.");
            let idx = ((max - min) / 2) + min;
            match &target.cmp(&nums[idx as usize]) {
                Ordering::Less => {
                    max = idx - 1;
                },
                Ordering::Equal => {
                    return idx;
                },
                Ordering::Greater => {
                    min = idx + 1;
                }
            }
        }
        min
    }
}

pub fn run() {
    let nums = vec![1,3,5,6];
    let target = 5;
    println!("{}", target);
    assert_eq!(Solution::search_insert(nums, target), 2);

    let nums = vec![1,3,5,6];
    let target = 2;
    println!("{}", target);
    assert_eq!(Solution::search_insert(nums, target), 1);

    let nums = vec![1,3,5,6];
    let target = 7;
    println!("{}", target);
    assert_eq!(Solution::search_insert(nums, target), 4);

    let nums = vec![1,3,5,6];
    let target = 0;
    println!("{}", target);
    assert_eq!(Solution::search_insert(nums, target), 0);
}
