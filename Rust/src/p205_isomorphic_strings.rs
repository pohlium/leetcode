use crate::Solution;

impl Solution {
    pub fn is_isomorphic(s: String, t: String) -> bool {
        let mut c_map = std::collections::BTreeMap::new();
        let mut used_vals = std::collections::BTreeSet::new();
        // by constraint, we know that s.len() == t.len()
        for (char_s, char_t) in s.chars().zip(t.chars()) {
            if let Some(mapped_char_s) = c_map.get(&char_s) {
                if *mapped_char_s != char_t {
                    return false;
                }
            } else if used_vals.contains(&char_t) {
                return false;
            } else {
                c_map.insert(char_s, char_t);
                used_vals.insert(char_t);
            }
        }
        true
    }
}

pub fn run() {
    let s = "egg".to_string();
    let t = "add".to_string();
    assert!(Solution::is_isomorphic(s, t));

    let s = "foo".to_string();
    let t = "bar".to_string();
    assert!(!Solution::is_isomorphic(s, t));

    let s = "paper".to_string();
    let t = "title".to_string();
    assert!(Solution::is_isomorphic(s, t));

    let s = "badc".to_string();
    let t = "baba".to_string();
    assert!(!Solution::is_isomorphic(s, t));
}
