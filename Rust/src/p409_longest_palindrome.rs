/// Problem 409: Given a string `s`, what is the length of the longest (case sensitive!)
/// palindrome you can build with the characters from s?
use crate::Solution;

impl Solution {
    pub fn longest_palindrome(s: String) -> i32 {
        use std::collections::HashMap;
        if s.len() == 1 {
            return 1;
        }
        let mut letters: HashMap<char, i32> = HashMap::new();
        for c in s.chars() {
            letters.entry(c).and_modify(|cnt| *cnt += 1).or_insert(1);
        }
        let mut sum = 0;
        let mut has_odd = false;
        for cnt in letters.values() {
            if cnt % 2 == 0 {
                sum += cnt;
            } else {
                sum += cnt - 1;
                has_odd = true;
            }
        }
        if has_odd {
            sum + 1
        } else {
            sum
        }
    }
}

pub fn run() {
    assert_eq!(Solution::longest_palindrome("abab".to_owned()), 4);
    assert_eq!(Solution::longest_palindrome("cabab".to_owned()), 5);
    assert_eq!(Solution::longest_palindrome("ababcd".to_owned()), 4);
    assert_eq!(Solution::longest_palindrome("cababcd".to_owned()), 6);
    assert_eq!(Solution::longest_palindrome("cabdabcd".to_owned()), 8);
    assert_eq!(Solution::longest_palindrome("a".to_owned()), 1);
}
