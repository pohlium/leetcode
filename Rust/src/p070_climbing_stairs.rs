/// Problem 70: Climbing stairs. You get a number n of stairs and per step you can either climb one
/// or two steps. How many distinct ways are there to climb the stairs?
use crate::Solution;

impl Solution {
    pub fn climb_stairs_070(n: i32) -> i32 {
        // Iterative fib
        if n == 0 || n == 1 {
            1
        } else if n == 2 {
            2
        } else {
            let mut pre_previous = 1;
            let mut previous = 2;
            let mut current = 3;
            for _ in 3..=n {
                current = previous + pre_previous;
                pre_previous = previous;
                previous = current;
            }
            current
        }
    }
}

pub fn run() {
    assert_eq!(Solution::climb_stairs_070(1), 1);
    assert_eq!(Solution::climb_stairs_070(2), 2);
    assert_eq!(Solution::climb_stairs_070(3), 3);
    assert_eq!(Solution::climb_stairs_070(4), 5);
    assert_eq!(Solution::climb_stairs_070(5), 8);
}
