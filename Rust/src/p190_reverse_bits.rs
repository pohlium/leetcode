use crate::Solution;

impl Solution {
    pub fn reverse_bits(x: u32) -> u32 {
        // u32::from_str_radix(&format!("{x:032b}").chars().rev().collect::<String>(), 2).unwrap()
        x.reverse_bits()
    }
}

pub fn run() {
    let n = 43261596;
    assert_eq!(Solution::reverse_bits(n), 964176192);

    let n = 4294967293;
    assert_eq!(Solution::reverse_bits(n), 3221225471);
}
