use crate::Solution;

use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<Self>>>,
    pub right: Option<Rc<RefCell<Self>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

type TNode = Option<Rc<RefCell<TreeNode>>>;

impl Solution {
    pub fn sorted_array_to_bst(nums: Vec<i32>) -> TNode {
        if nums.is_empty() {
            return None;
        }
        let middle_index = nums.len() / 2;
        let middle_node = Rc::new(RefCell::new(TreeNode::new(nums[middle_index])));
        // With a helper function that takes &Vec<i32> or &[i32] we could get rid of this
        // `.to_vec()` to increase performance / decrease allocations
        middle_node.borrow_mut().left = Self::sorted_array_to_bst(nums[..middle_index].to_vec());
        middle_node.borrow_mut().right = Self::sorted_array_to_bst(nums[middle_index + 1..].to_vec());
        Some(middle_node)
    }
}

pub fn run() {}
