use crate::Solution;

impl Solution {
    pub fn contains_nearby_duplicate(nums: Vec<i32>, k: i32) -> bool {
        use std::collections::BTreeMap;
        let k = k as usize;
        let mut indice_map: BTreeMap<i32, usize> = BTreeMap::new();
        for (i, n) in nums.into_iter().enumerate() {
            let j = indice_map.entry(n).or_insert(i);
            if *j != i && i - *j <= k {
                return true;
            } else {
                // Hugely illegal modification of the entry value here!
                *j = i;
            }
        }
        false
    }
}

pub fn run() {
    let nums = vec![1,2,3,1];
    let k = 3;
    assert!(Solution::contains_nearby_duplicate(nums, k));

    let nums = vec![1,2,3,1,2,3];
    let k = 2;
    assert!(!Solution::contains_nearby_duplicate(nums, k));

    let nums = vec![1,0,1,1];
    let k = 1;
    assert!(Solution::contains_nearby_duplicate(nums, k));
}
