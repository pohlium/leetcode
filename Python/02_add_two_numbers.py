from __future__ import annotations
from typing import Optional

class ListNode:
    def __init__(self, x: int):
        self.val = x
        self.next: Optional[ListNode] = None

    def print(self):
        mul = 10
        sum = self.val
        p = self.next
        while p is not None:
            sum += mul * p.val
            mul *= 10
            p = p.next
        print(sum)


class Solution:
    def build_list(self, s: int) -> ListNode:
        resp = ListNode(s % 10)
        append_to = resp
        s = s // 10
        while s != 0:
            digit = s % 10
            s = s // 10
            temp_node = ListNode(digit)
            append_to.next = temp_node
            append_to = append_to.next
        return resp

    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        s = 0
        i = 0
        # get numbers from list
        current_node = l1
        while current_node.next:
            s += current_node.val * (10 ** i)
            current_node = current_node.next
            i += 1
        s += current_node.val * (10 ** i)
        current_node = l2
        i = 0
        while current_node.next:
            s += current_node.val * (10 ** i)
            current_node = current_node.next
            i += 1
        s += current_node.val * (10 ** i)
        # put solution in list
        return self.build_list(s)

    def addTwoNumbersReal(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        if l1 is None or l2 is None:
            return None

        sum = l1.val + l2.val
        carry = sum // 10
        sum = sum % 10
        result = ListNode(sum)
        ptr = result

        l1 = l1.next
        l2 = l2.next

        while l1 is not None or l2 is not None:
            v1 = l1.val if l1 is not None else 0
            v2 = l2.val if l2 is not None else 0
            print(f'DBG: adding {v1} and {v2} + carry {carry}')
            sum = v1 + v2 + carry
            print(f'DBG: Presum: {sum}')
            carry = sum // 10
            print(f'DBG: Carry: {carry}')
            sum = sum % 10
            print(f'DBG: Sum: {sum}')
            ptr.next = ListNode(sum)
            ptr = ptr.next
            l1 = l1.next if l1 is not None else None
            l2 = l2.next if l2 is not None else None

        if carry > 0:
            print(f'DBG: adding last node with {carry}')
            ptr.next = ListNode(carry)

        return result

if __name__ == '__main__':
    s = Solution()
    a = s.build_list(9999999)
    b = s.build_list(9999)
    a.print()
    b.print()
    r = s.addTwoNumbersReal(a, b)
    if r is not None:
        r.print()
    else:
        print('No result.')

