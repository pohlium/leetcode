use crate::Solution;

impl Solution {
    pub fn is_valid(s: String) -> bool {
        let mut stack = Vec::new();
        for c in s.chars() {
            match c {
                '(' | '[' | '{' => stack.push(c),
                ')' => {
                    if stack.pop() != Some('(') {
                        return false;
                    }
                },
                ']' => {
                    if stack.pop() != Some('[') {
                        return false;
                    }
                },
                '}' => {
                    if stack.pop() != Some('{') {
                        return false;
                    }
                },
                _ => { panic!("This cannot happen.")}
            }
        }
        stack.is_empty()
    }
}

pub fn run() {
    let s = "()".to_string();
    assert!(Solution::is_valid(s));

    let s = "()[]{}".to_string();
    assert!(Solution::is_valid(s));

    let s = "(]".to_string();
    assert!(!Solution::is_valid(s));
}
