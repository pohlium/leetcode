use crate::Solution;

impl Solution {
    pub fn max_profit(prices: Vec<i32>) -> i32 {
        let mut min = prices[0]; // as constraing we know prices.len() >= 1
        let mut largest_profit = 0;
        for price in prices {
            // This could be min and max functions, but i believe this is actually faster (though
            // perhap rust optimizes it away?)
            if price - min > largest_profit {
                largest_profit = price - min;
            }
            if price < min {
                min = price;
            }
        }
        largest_profit
    }
}

pub fn run() {
    let prices = vec![7,1,5,3,6,4];
    assert_eq!(Solution::max_profit(prices), 5);

    let prices = vec![7,6,4,3,1];
    assert_eq!(Solution::max_profit(prices), 0);
}
