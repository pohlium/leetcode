use crate::Solution;

use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

impl Solution {
    fn dfs(n1: &Option<Rc<RefCell<TreeNode>>>, n2: &Option<Rc<RefCell<TreeNode>>>) -> bool {
        match (n1, n2) {
            (Some(n1), Some(n2)) => {
                n1.borrow().val == n2.borrow().val &&
                    Self::dfs(&n1.borrow().left, &n2.borrow().right) &&
                    Self::dfs(&n1.borrow().right, &n2.borrow().left)
            }
            (None, None) => true,
            (_, _) => false
        }
    }

    pub fn is_symmetric(root: Option<Rc<RefCell<TreeNode>>>) -> bool {
        Self::dfs(&root, &root)
    }
}

pub fn run() {

}
