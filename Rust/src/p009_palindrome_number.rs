use crate::Solution;

impl Solution {
    pub fn is_palindrome_number(x: i32) -> bool {
        if x.is_negative() {
            return false;
        }
        let mut x = x;
        let mut digits = Vec::with_capacity((x as f32).log10().round() as usize);
        while x != 0 {
            digits.push(x % 10);
            x /= 10;
        }
        digits.iter().eq(digits.iter().rev())
    }
}

pub fn run() {
    let x = 121;
    assert!(Solution::is_palindrome_number(x));

    let x = -121;
    assert!(!Solution::is_palindrome_number(x));

    let x = 10;
    assert!(!Solution::is_palindrome_number(x));
}
