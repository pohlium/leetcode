use crate::Solution;

use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

impl Solution {
    pub fn invert_tree(root: Option<Rc<RefCell<TreeNode>>>) -> Option<Rc<RefCell<TreeNode>>> {
        if let Some(ref tree) = root {
            let tree = &mut *tree.borrow_mut();
            std::mem::swap(&mut tree.left, &mut tree.right);
            Solution::invert_tree(tree.left.clone());
            Solution::invert_tree(tree.right.clone());
        }
        root
    }
}

pub fn run() {
    // Building big trees is so much effort, so i'm leaving out the local test cases here.
    // This only tests that at least inverting does not crash.
    let p_rleaf = Some(Rc::new(RefCell::new(TreeNode::new(3))));
    let p_lleaf = Some(Rc::new(RefCell::new(TreeNode::new(2))));
    let p = Some(Rc::new(RefCell::new(TreeNode::new(1))));
    p.clone().unwrap().borrow_mut().right = p_rleaf;
    p.clone().unwrap().borrow_mut().left = p_lleaf;

    let q_rleaf = Some(Rc::new(RefCell::new(TreeNode::new(3))));
    let q_lleaf = Some(Rc::new(RefCell::new(TreeNode::new(2))));
    let q = Some(Rc::new(RefCell::new(TreeNode::new(1))));
    q.clone().unwrap().borrow_mut().right = q_rleaf;
    q.clone().unwrap().borrow_mut().left = q_lleaf;

    Solution::invert_tree(p);
}
