use crate::Solution;

impl Solution {
    pub fn hamming_weight(n: u32) -> i32 {
        n.count_ones() as i32
    }
}

pub fn run() {
    let n = 11;
    assert_eq!(Solution::hamming_weight(n), 3);

    let n = 128;
    assert_eq!(Solution::hamming_weight(n), 1);

    let n = 4294967293;
    assert_eq!(Solution::hamming_weight(n), 31);
}
