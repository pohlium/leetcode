use crate::Solution;

impl Solution {
    pub fn merge(nums1: &mut Vec<i32>, m: i32, nums2: &mut Vec<i32>, n: i32) {
        let mut ptr_nums1 = m - 1;
        let mut ptr_nums2 = n - 1;
        let mut ptr_insert = m + n - 1;
        while ptr_nums1 >= 0 && ptr_nums2 >= 0 {
            if nums1[ptr_nums1 as usize] > nums2[ptr_nums2 as usize] {
                nums1[ptr_insert as usize] = nums1[ptr_nums1 as usize];
                ptr_nums1 -= 1;
            } else {
                nums1[ptr_insert as usize] = nums2[ptr_nums2 as usize];
                ptr_nums2 -= 1;
            }
            ptr_insert -= 1;
        }
        while ptr_nums2 >= 0 {
            nums1[ptr_insert as usize] = nums2[ptr_nums2 as usize];
            ptr_nums2 -= 1;
            ptr_insert -= 1;
        }
    }
}

fn test(nums1: &mut Vec<i32>, nums2: &mut Vec<i32>) {
    let l1 = nums1.len();
    let l2 = nums2.len();
    Solution::merge(nums1, (l1 - l2) as i32, nums2, l2 as i32);
    println!("{:?}", nums1);
}

pub fn run() {
    let mut nums1 = vec![1, 2, 3, 0, 0, 0];
    let mut nums2 = vec![2, 5, 6];
    test(&mut nums1, &mut nums2);

    let mut nums1 = vec![1];
    let mut nums2 = vec![];
    test(&mut nums1, &mut nums2);

    let mut nums1 = vec![0];
    let mut nums2 = vec![1];
    test(&mut nums1, &mut nums2);
}
