use crate::Solution;

impl Solution {
    pub fn length_of_last_word(s: String) -> i32 {
        s.trim().split(' ').next_back().unwrap().len() as i32
    }
}

pub fn run() {
    assert_eq!(Solution::length_of_last_word("Hello World".to_string()), 5);
    assert_eq!(Solution::length_of_last_word("   fly me   to   the moon  ".to_string()), 4);
    assert_eq!(Solution::length_of_last_word("luffy is still joyboy".to_string()), 6);
}
