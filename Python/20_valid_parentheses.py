class Solution:
    def isValid(self, s: str) -> bool:
        stack = []
        par = {
          ")": "(",
          "]": "[",
          "}": "{"
        }
        for char in s:
          if char in par.values():
            stack.append(char)
          else:
            if not stack or par[char] != stack.pop():
              return False
        return False if stack else True
