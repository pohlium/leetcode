// p278 find first bad version for version 1..n given a number n.
// I believe this is just binary search?
use crate::Solution;

impl Solution {
    pub fn isBadVersion(&self, version: i32) -> bool {
        // this is an unknown function provided by leetcode
        todo!()
    }

    pub fn first_bad_version_278(&self, n: i32) -> i32 {
        let mut lower_bound = 1;
        let mut upper_bound = n;
        // This is always the middle between the bounds ROUNDED UP for even n;
        let mut current = lower_bound + (upper_bound - lower_bound) / 2;
        while lower_bound <= upper_bound {
            if self.isBadVersion(current) {
                upper_bound = current - 1;
            } else {
                lower_bound = current + 1;
            }
            current = lower_bound + (upper_bound - lower_bound) / 2;
        }
        upper_bound + 1
    }
}

pub fn run() {
    // can't run because we don't know the `isBadVersion` function.
}
