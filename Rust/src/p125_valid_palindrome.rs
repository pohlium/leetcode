use crate::Solution;

impl Solution {
    pub fn is_palindrome(s: String) -> bool {
        let s: Vec<char> = s.to_lowercase().chars().filter(|c| c.is_alphanumeric()).collect();
        for (c1, c2) in s.iter().zip(s.iter().rev()) {
            if c1 != c2 {
                return false;
            }
        }
        true
    }
}

pub fn run() {
    let s = "A man, a plan, a canal: Panama".to_string();
    assert!(Solution::is_palindrome(s));

    let s = "race a car".to_string();
    assert!(!Solution::is_palindrome(s));

    let s = " ".to_string();
    assert!(Solution::is_palindrome(s));
}
