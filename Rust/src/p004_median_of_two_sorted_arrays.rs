use crate::Solution;
use std::collections::HashMap;

const MAX: i32 = 10000000;

fn single_list_median(nums: Vec<i32>) -> f64 {
    let length = nums.len();
    if length % 2 == 0 {
        (nums[length / 2] as f64 + nums[length / 2 - 1] as f64) / 2.0
    } else {
        nums[length / 2] as f64
    }
}

impl Solution {
    fn find_median_sorted_arrays(nums1: Vec<i32>, nums2: Vec<i32>) -> f64 {
        if nums1.is_empty() {
            return single_list_median(nums2);
        } else if nums2.is_empty() {
            return single_list_median(nums1);
        }

        let target_idx = (nums1.len() + nums2.len()) / 2;
        let have_to_avg = (nums1.len() + nums2.len()) % 2 == 0;

        const NUMS1_ID: i32 = 0;
        const NUMS2_ID: i32 = !NUMS1_ID;

        let mut lists = HashMap::new();
        lists.insert(NUMS1_ID, &nums1);
        lists.insert(NUMS2_ID, &nums2);

        let mut cur_list_id = if nums1[0] < nums2[0] {
            NUMS1_ID
        } else {
            NUMS2_ID
        };

        let mut idxs: HashMap<i32, i32> = HashMap::new();
        idxs.insert(cur_list_id, 0);
        idxs.insert(!cur_list_id, -1);

        let mut prev_list_id = 0;
        let mut prev_idx = 0;

        for _ in 0..target_idx {
            let next_i = if nums1.len() > (idxs[&NUMS1_ID] + 1) as usize {
                nums1[(idxs[&NUMS1_ID] + 1) as usize]
            } else {
                MAX
            };
            println!("Next i: {}", next_i);

            let next_j = if nums2.len() > (idxs[&NUMS2_ID] + 1) as usize {
                nums2[(idxs[&NUMS2_ID] + 1) as usize]
            } else {
                MAX
            };
            println!("Next j: {}", next_j);

            prev_list_id = cur_list_id;
            prev_idx = idxs[&cur_list_id];

            if next_i < next_j {
                idxs.entry(NUMS1_ID).and_modify(|idx| *idx += 1);
                cur_list_id = NUMS1_ID;
            } else {
                idxs.entry(NUMS2_ID).and_modify(|idx| *idx += 1);
                cur_list_id = NUMS2_ID;
            };
            println!("Current list: {:?}", lists[&cur_list_id]);
            println!("Current idx: {}", idxs[&cur_list_id]);
            println!(
                "Current item: {}",
                lists[&cur_list_id][idxs[&cur_list_id] as usize]
            );
        }

        if have_to_avg {
            (lists[&cur_list_id][idxs[&cur_list_id] as usize]
                + lists[&prev_list_id][prev_idx as usize]) as f64
                / 2.0
        } else {
            lists[&cur_list_id][idxs[&cur_list_id] as usize] as f64
        }
    }
}

pub fn run() {
    let m = Solution::find_median_sorted_arrays(vec![0, 7, 8], vec![1, 2, 3, 4, 5, 6]);
    println!("{}", m);
}
