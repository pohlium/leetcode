use crate::Solution;

impl Solution {
    pub fn plus_one(digits: Vec<i32>) -> Vec<i32> {
        let mut digits = digits;
        for digit in digits.iter_mut().rev() {
            *digit = (*digit + 1) % 10;
            if *digit != 0 {
                return digits;
            }
        }
        digits.insert(0, 1);
        digits
    }
}

pub fn run() {
    let digits = vec![1, 2, 3];
    assert_eq!(Solution::plus_one(digits), vec![1, 2, 4]);

    let digits = vec![4, 3, 2, 1];
    assert_eq!(Solution::plus_one(digits), vec![4, 3, 2, 2]);

    let digits = vec![9];
    assert_eq!(Solution::plus_one(digits), vec![1, 0]);
}
