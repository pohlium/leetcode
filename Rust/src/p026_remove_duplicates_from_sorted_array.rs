use crate::Solution;

impl Solution {
    fn remove_duplicates(nums: &mut Vec<i32>) -> i32 {
        nums.dedup();
        nums.len() as i32
    }
}

pub fn run() {
    let mut nums = vec![1,2,2,2,4,4,5,6,7,8,8,8,8,9];
    Solution::remove_duplicates(&mut nums);
    assert_eq!(nums, [1,2,4,5,6,7,8,9]);
}
