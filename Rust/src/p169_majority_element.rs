use crate::Solution;

impl Solution {
    pub fn majority_element(nums: Vec<i32>) -> i32 {
        let mut count = 1;
        let mut current_element = nums[0];
        for n in nums.into_iter().skip(1) {
            if current_element == n {
                count += 1;
            } else if count == 0 {
                current_element = n;
                count = 1;
            } else {
                count -= 1;
            }
        }
        current_element
    }
}

pub fn run() {
    let nums = vec![3,2,3];
    assert_eq!(Solution::majority_element(nums), 3);

    let nums = vec![2,2,1,1,1,2,2];
    assert_eq!(Solution::majority_element(nums), 2);
}
