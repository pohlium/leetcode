use crate::Solution;

impl Solution {
    pub fn search_704(nums: Vec<i32>, target: i32) -> i32 {
        let mut lower_bound = 0;
        let mut upper_bound = nums.len();
        // This is always the middle between the bounds ROUNDED UP for vectors with even amount
        // of elements.
        let mut current = lower_bound + (upper_bound - lower_bound) / 2;
        while nums[current] != target && (upper_bound - lower_bound) > 1 {
            if nums[current] > target {
                upper_bound = current;
            } else {
                lower_bound = current;
            }
            current = lower_bound + (upper_bound - lower_bound) / 2;
        }
        if nums[current] == target {
            current as i32
        } else if nums[lower_bound] == target {
            upper_bound as i32
        } else {
            -1
        }
    }

    pub fn search_704_rusty(nums: Vec<i32>, target: i32) -> i32 {
        use std::cmp::Ordering;
        let (mut lower_bound, mut upper_bound) = (0, nums.len());
        while lower_bound < upper_bound {
            let current = lower_bound + (upper_bound - lower_bound) / 2;
            match nums[current].cmp(&target) {
                Ordering::Less => lower_bound = current + 1,
                Ordering::Equal => return current as i32,
                Ordering::Greater => upper_bound = current,
            }
        }
        -1
    }
}

pub fn run() {
    assert_eq!(Solution::search_704(vec![-1, 0, 3, 5, 9, 12], 9), 4);
    assert_eq!(Solution::search_704(vec![-1, 0, 3, 5, 9, 12], 2), -1);
    assert_eq!(Solution::search_704(vec![5], 5), 0);
    assert_eq!(Solution::search_704(vec![5], -5), -1);
}
