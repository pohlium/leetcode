use crate::Solution;

impl Solution {
    pub fn word_pattern(pattern: String, s: String) -> bool {
        let mut word_to_letter = std::collections::BTreeMap::new();
        let mut letter_to_word = std::collections::BTreeMap::new();
        let words: Vec<&str> = s.split(' ').collect();
        if words.len() != pattern.len() {
            return false;
        }
        for (letter, word) in pattern.chars().zip(words) {
            if let Some(existing_letter) = word_to_letter.insert(word, letter) {
                if existing_letter != letter {
                    return false;
                }
            }
            if let Some(existing_word) = letter_to_word.insert(letter, word) {
                if existing_word != word {
                    return false;
                }
            }
        }
        true
    }
}

pub fn run() {
    let pattern = "abba".to_string();
    let s = "dog cat cat dog".to_string();
    assert!(Solution::word_pattern(pattern, s));

    let pattern = "abba".to_string();
    let s = "dog cat cat fish".to_string();
    assert!(!Solution::word_pattern(pattern, s));

    let pattern = "aaaa".to_string();
    let s = "dog cat cat dog".to_string();
    assert!(!Solution::word_pattern(pattern, s));
}
