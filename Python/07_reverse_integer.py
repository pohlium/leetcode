class Solution:
    def reverse(self, x: int) -> int:
      negative = -1 if x < 0 else 1
      y = str(x)
      if y.startswith("-"):
        y = y[1:]
      y = y[::-1]
      solution = int(y) * negative
      if solution < -2 ** 31 or solution > 2 ** 31 -1:
        return 0
      return solution
