use crate::Solution;

impl Solution {
    pub fn find_min(nums: Vec<i32>) -> i32 {
        let middle_idx = nums.len() / 2;
        // Basically binarysearch.
        // When we go right and we get a larger number than before, we want to look into the right
        // half of the array.
        // If we go right and get a smaller number than before, we want to go left next.
        // And vice versa.
        0
    }

    /// We don't want to do a recursive algorithm, ideally we just want to do an iterative one
    /// where we change the bounds indices.
    pub fn divide(v: &[i32]) {
        // When have we found the correct number?
        // Either the number to the right (current idx + 1) is *smaller* than the current one, or
        // we are at the right-most index.
    }
}

pub fn run() {
    Solution::find_min(vec![1, 2, 3, 4, 5, 6, 7, 8]);
}
