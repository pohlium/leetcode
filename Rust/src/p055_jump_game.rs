use crate::Solution;

fn _can_jump_solution(nums: Vec<i32>) -> bool {
    let mut required_jump = 1;
    // iterate from the second to last element of nums
    // we know that here, we need at least a jump of 1 to reach last.
    // if we have a 0, we know that for the previous element, we need to have a jump length of at
    // least 2.
    // if we can reach that, we can reset the required jump length to 1 again for the element
    // before that, otherwise we bump it up to 3 to jump straight to last.
    for jump in nums.into_iter().rev().skip(1) {
        // if we can reach the last element that we know is able to (transitively) jump to last,
        // then we can set the required jump for the next element to 1, otherwise we know that
        // from the previous element, we need to be able to jump at least one more step
        required_jump = if jump >= required_jump {
            1
        } else {
            required_jump + 1
        };
    }
    required_jump == 1
}

impl Solution {
    pub fn can_jump(nums: Vec<i32>) -> bool {
        let target = (nums.len() - 1) as i32;
        let mut available_indexes = std::collections::BTreeSet::new();
        available_indexes.insert(0);

        // LeetCode's Rust compiler is too old to support pop_first(), so we always iter once and
        // then remove that element inside the loop.
        // while let Some(current_idx) = available_indexes.pop_first() {
        while let Some(current_idx) = available_indexes.iter().next().cloned() {
            available_indexes.remove(&current_idx);
            let max_idx = current_idx + nums[current_idx as usize];
            if max_idx >= target {
                return true;
            }
            for i in (current_idx + 1)..=max_idx {
                available_indexes.insert(i);
            }
        }
        false
    }
}

pub fn run() {
    let nums = vec![2, 3, 1, 1, 4];
    assert!(Solution::can_jump(nums));

    let nums = vec![3, 2, 1, 0, 4];
    assert!(!Solution::can_jump(nums));

    let nums = vec![
        2, 0, 6, 9, 8, 4, 5, 0, 8, 9, 1, 2, 9, 6, 8, 8, 0, 6, 3, 1, 2, 2, 1, 2, 6, 5, 3, 1, 2, 2,
        6, 4, 2, 4, 3, 0, 0, 0, 3, 8, 2, 4, 0, 1, 2, 0, 1, 4, 6, 5, 8, 0, 7, 9, 3, 4, 6, 6, 5, 8,
        9, 3, 4, 3, 7, 0, 4, 9, 0, 9, 8, 4, 3, 0, 7, 7, 1, 9, 1, 9, 4, 9, 0, 1, 9, 5, 7, 7, 1, 5,
        8, 2, 8, 2, 6, 8, 2, 2, 7, 5, 1, 7, 9, 6,
    ];
    assert!(!Solution::can_jump(nums));
}
