use crate::Solution;

#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

use std::cell::RefCell;
use std::rc::Rc;
impl Solution {
    // We know that tree node values are *unique*, so we can uniquely identify a node by its value.
    // We know that all our inputs are not None.
    // We know that p != q and that both exist as descendants of root (where any node is a
    // descendant of itself)
    pub fn lowest_common_ancestor_235(
        root: Option<Rc<RefCell<TreeNode>>>,
        p: Option<Rc<RefCell<TreeNode>>>,
        q: Option<Rc<RefCell<TreeNode>>>,
    ) -> Option<Rc<RefCell<TreeNode>>> {
        use std::cmp::Ordering;
        let root_val = root.as_ref().unwrap().borrow().val;
        let p_val = p.as_ref().unwrap().borrow().val;
        let q_val = q.as_ref().unwrap().borrow().val;
        match (root_val.cmp(&p_val), root_val.cmp(&q_val)) {
            (Ordering::Greater, Ordering::Greater) => {
                Solution::lowest_common_ancestor_235(root.unwrap().borrow().left.clone(), p, q)
            }
            (Ordering::Less, Ordering::Less) => {
                Solution::lowest_common_ancestor_235(root.unwrap().borrow().right.clone(), p, q)
            }
            (_, _) => root,
        }
    }
}

pub fn run() {
    // Build Tree
    // Assert Eq that the LCA is the correct node
    // Solution::lowest_common_ancestor_235(None, None, None);
    // Building trees is too hard :(
}
