/// p 110 determine whether given binary tree is height balanced
use crate::Solution;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

impl Solution {
    pub fn is_balanced_110(root: Option<Rc<RefCell<TreeNode>>>) -> bool {
        pub fn dfs(root: &Option<Rc<RefCell<TreeNode>>>) -> i32 {
            if let Some(node) = root {
                let height_left = dfs(&node.borrow().left);
                let height_right = dfs(&node.borrow().right);
                if height_left.abs_diff(height_right) > 1 || height_left < 0 || height_right < 0 {
                    -1
                } else {
                    height_right.max(height_left) + 1
                }
            } else {
                0
            }
        }
        dfs(&root) != -1
    }
}

pub fn run() {
    // building trees is hard so we don't test this locally.
}
