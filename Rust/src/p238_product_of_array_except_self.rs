use crate::Solution;

impl Solution {
    pub fn product_except_self(nums: Vec<i32>) -> Vec<i32> {
        let mut prefix_arr = Vec::with_capacity(nums.len());
        let mut prefix = 1;

        for num in nums.iter().copied() {
            prefix_arr.push(prefix);
            prefix *= num;
        }

        let mut postfix = 1;
        for i in (0..nums.len()).rev() {
            prefix_arr[i] *= postfix;
            postfix *= nums[i];
        }
        prefix_arr
    }
}

pub fn run() {
    let nums = vec![1,2,3,4];
    assert_eq!(Solution::product_except_self(nums), [24,12,8,6]);

    let nums = vec![-1,1,0,-3,3];
    assert_eq!(Solution::product_except_self(nums), [0,0,9,0,0]);
}
