use crate::Solution;

impl Solution {
    pub fn can_construct(ransom_note: String, magazine: String) -> bool {
        let mut ransom_note: Vec<char> = ransom_note.chars().collect();
        ransom_note.sort_unstable();

        let mut magazine: Vec<char> = magazine.chars().collect();
        magazine.sort_unstable();

        // implement is_subsequence, but this time with 2 pointers (because it's fun);
        let mut r_ptr = 0;
        for m_chr in magazine {
            if m_chr == ransom_note[r_ptr] {
                r_ptr += 1;
                if r_ptr >= ransom_note.len() {
                    return true;
                }
            }
        }
        r_ptr == ransom_note.len()
    }
}

pub fn run() {
    let ransom_note = "a".to_string();
    let magazine = "b".to_string();
    assert!(!Solution::can_construct(ransom_note, magazine));

    let ransom_note = "aa".to_string();
    let magazine = "ab".to_string();
    assert!(!Solution::can_construct(ransom_note, magazine));

    let ransom_note = "aa".to_string();
    let magazine = "aab".to_string();
    assert!(Solution::can_construct(ransom_note, magazine));
}
