use crate::Solution;

impl Solution {
    pub fn max_profit_II(prices: Vec<i32>) -> i32 {
        /* My own solution
        let mut profit = 0;
        for i in 0..(prices.len() - 1) {
            let daily_profit = prices[i + 1] - prices[i];
            if daily_profit > 0 {
                profit += daily_profit;
            }
        }
        profit
        */

        // Cool functional one-liner solution
        prices.windows(2).map(|window| (window[1] - window[0]).max(0)).sum()
    }
}

pub fn run() {
    let prices = vec![7, 1, 5, 3, 6, 4];
    assert_eq!(Solution::max_profit_II(prices), 7);

    let prices = vec![1, 2, 3, 4, 5];
    assert_eq!(Solution::max_profit_II(prices), 4);

    let prices = vec![7, 6, 4, 3, 1];
    assert_eq!(Solution::max_profit_II(prices), 0);
}
