use crate::Solution;

impl Solution {
    pub fn adjacent_pixels_733(
        row: usize,
        col: usize,
        rows: usize,
        cols: usize,
    ) -> Vec<(usize, usize)> {
        let mut pixels = Vec::new();
        if row > 0 {
            pixels.push((row - 1, col));
        };
        if row < rows - 1 {
            pixels.push((row + 1, col));
        };
        if col > 0 {
            pixels.push((row, col - 1));
        };
        if col < cols - 1 {
            pixels.push((row, col + 1));
        }
        pixels
    }

    pub fn flood_fill_733_recursive(
        image: &mut Vec<Vec<i32>>,
        start_row: usize,
        start_col: usize,
        color: i32,
    ) {
        let original_color = image[start_row][start_col];
        if original_color == color {
            return;
        }
        image[start_row][start_col] = color;
        for (row, col) in
            Solution::adjacent_pixels_733(start_row, start_col, image.len(), image[0].len())
        {
            if image[row][col] == original_color {
                Solution::flood_fill_733_recursive(image, row as _, col as _, color);
            }
        }
    }

    // You could also implement this "iteratively" by creating your own "stack" of cells that you
    // push all the new neighbors to, and then pop them in a loop, adding their respective
    // neighbors within that loop to the stack if necessary.
    pub fn flood_fill_733(mut image: Vec<Vec<i32>>, sr: i32, sc: i32, color: i32) -> Vec<Vec<i32>> {
        Solution::flood_fill_733_recursive(&mut image, sr as _, sc as _, color);
        image
    }
}

pub fn run() {
    assert_eq!(
        Solution::flood_fill_733(vec![vec![1, 1, 1], vec![1, 1, 0], vec![1, 0, 1]], 1, 1, 2),
        vec![vec![2, 2, 2], vec![2, 2, 0], vec![2, 0, 1]]
    );
    assert_eq!(
        Solution::flood_fill_733(vec![vec![0, 0, 0], vec![0, 0, 0]], 0, 0, 0),
        vec![vec![0, 0, 0], vec![0, 0, 0]]
    );
}
