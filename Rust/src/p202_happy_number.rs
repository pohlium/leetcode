use crate::Solution;

impl Solution {
    pub fn is_happy(mut n: i32) -> bool {
        let mut seen = std::collections::BTreeSet::new();
        while n != 1 {
            if !seen.insert(n) {
                return false;
            }
            let mut x = n;
            let mut sum = 0;
            while x != 0 {
                sum += (x % 10).pow(2);
                x /= 10;
            }
            n = sum;
        }
        true
    }
}

pub fn run() {
    assert!(Solution::is_happy(19));
    assert!(!Solution::is_happy(2));
    assert!(Solution::is_happy(7));
}
