use crate::Solution;

use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

impl Solution {
    fn rec_path_sum(
        root: &Option<Rc<RefCell<TreeNode>>>,
        current_sum: i32,
        target_sum: i32,
    ) -> bool {
        if let Some(tree) = root {
            let node_sum = current_sum + tree.borrow().val;
            match (&tree.borrow().left, &tree.borrow().right) {
                (None, None) => node_sum == target_sum,
                (left, right) => {
                    Self::rec_path_sum(left, node_sum, target_sum)
                        || Self::rec_path_sum(right, node_sum, target_sum)
                }
            }
        } else {
            false
        }
    }

    pub fn has_path_sum(root: Option<Rc<RefCell<TreeNode>>>, target_sum: i32) -> bool {
        Self::rec_path_sum(&root, 0, target_sum)
    }
}

pub fn run() {}
