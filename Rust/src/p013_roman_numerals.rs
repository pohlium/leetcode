use crate::Solution;

impl Solution {
    pub fn roman_to_int(s: String) -> i32 {
        let numeral_map = std::collections::HashMap::from([
            ('I', 1),
            ('V', 5),
            ('X', 10),
            ('L', 50),
            ('C', 100),
            ('D', 500),
            ('M', 1000),
        ]);
        let nums: Vec<i32> = s
            .chars()
            .map(|c| numeral_map.get(&c).copied().unwrap())
            .collect();
        let mut previous_num = 0;
        let mut sum = 0;
        for i in nums.into_iter().rev() {
            sum += if previous_num > i { -i } else { i };
            previous_num = i;
        }
        sum
    }
}

pub fn run() {
    assert_eq!(Solution::roman_to_int("III".to_string()), 3);
    assert_eq!(Solution::roman_to_int("MCMXCVII".to_string()), 1997);
}
