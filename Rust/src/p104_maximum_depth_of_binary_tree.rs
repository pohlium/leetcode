use crate::Solution;

use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug, PartialEq, Eq)]
pub struct TreeNode {
    pub val: i32,
    pub left: Option<Rc<RefCell<TreeNode>>>,
    pub right: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        TreeNode {
            val,
            left: None,
            right: None,
        }
    }
}

impl Solution {
    fn recursive_max_depth(root: &Option<Rc<RefCell<TreeNode>>>) -> i32 {
        if let Some(root) = root {
            Solution::recursive_max_depth(&root.clone().borrow().left)
                .max(Solution::recursive_max_depth(&root.clone().borrow().right))
                + 1
        } else {
            0
        }
    }

    pub fn max_depth(root: Option<Rc<RefCell<TreeNode>>>) -> i32 {
        Solution::recursive_max_depth(&root)
    }
}

pub fn run() {
    let rleaf = Some(Rc::new(RefCell::new(TreeNode::new(7))));
    let mleaf = Some(Rc::new(RefCell::new(TreeNode::new(15))));
    let lleaf = Some(Rc::new(RefCell::new(TreeNode::new(9))));
    let rmid = Some(Rc::new(RefCell::new(TreeNode::new(20))));
    let head = Some(Rc::new(RefCell::new(TreeNode::new(3))));
    rmid.clone().unwrap().borrow_mut().left = mleaf;
    rmid.clone().unwrap().borrow_mut().right = rleaf;
    head.clone().unwrap().borrow_mut().right = rmid;
    head.clone().unwrap().borrow_mut().left = lleaf;

    assert_eq!(Solution::max_depth(head), 3);
}
