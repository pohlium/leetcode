use crate::Solution;

/// Up-Down-Peak strategy:
/// Collect streaks of ups and downs
/// remember position of last peak
/// the valleys get assigned 1, peaks of height N get assigned N
impl Solution {
    pub fn candy(ratings: Vec<i32>) -> i32 {
        let (mut ret, mut up, mut down, mut peak) = (1, 0, 0, 0);
        for i in 0..ratings.len() - 1 {
            let (prev, curr) = (ratings[i], ratings[i + 1]);
            match prev.cmp(&curr) {
                std::cmp::Ordering::Less => {
                    (up, down, peak, ret) = (up + 1, 0, up + 1, up + 2);
                }
                std::cmp::Ordering::Equal => {
                    (up, down, peak, ret) = (0, 0, 0, ret + 1);
                }
                std::cmp::Ordering::Greater => {
                    (up, down, ret) = (0, down + 1, ret + down + 1);
                    if peak >= down {
                        ret -= 1
                    };
                }
            }
        }
        ret
    }
}

pub fn run() {
    // let ratings = vec![1, 0, 2];
    // assert_eq!(Solution::candy(ratings), 5);

    // let ratings = vec![1, 2, 2];
    // assert_eq!(Solution::candy(ratings), 4);
}
