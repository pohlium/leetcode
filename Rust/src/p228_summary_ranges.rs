use crate::Solution;

impl Solution {
    pub fn summary_ranges(nums: Vec<i32>) -> Vec<String> {
        if nums.is_empty() {
            return vec![];
        }
        let mut prev = nums[0];
        let mut last_start = nums[0];
        let mut res = Vec::new();
        let last = *nums.last().unwrap();
        for i in nums.into_iter().skip(1) {
            if i != prev + 1 {
                res.push(if last_start == prev {
                    prev.to_string()
                } else {
                    format!("{last_start}->{prev}")
                });
                last_start = i;
            }
            prev = i;
        }
        res.push(if last_start == last {
            last.to_string()
        } else {
            format!("{last_start}->{last}")
        });
        res
    }
}

pub fn run() {
    let nums = vec![0, 1, 2, 4, 5, 7];
    assert_eq!(
        Solution::summary_ranges(nums),
        vec!["0->2".to_string(), "4->5".to_string(), "7".to_string()]
    );

    let nums = vec![0, 2, 3, 4, 6, 8, 9];
    assert_eq!(
        Solution::summary_ranges(nums),
        vec![
            "0".to_string(),
            "2->4".to_string(),
            "6".to_string(),
            "8->9".to_string()
        ]
    );
}
