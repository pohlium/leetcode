use crate::Solution;

impl Solution {
    pub fn add_binary(a: String, b: String) -> String {
        // This does bitwise addition, a nicer way to do this would be to work on 128-bit chunks at
        // once
        let mut s = Vec::with_capacity(a.len().max(b.len()) + 1);
        let mut carry = 0;
        let mut a_iter = a.chars().rev();
        let mut b_iter = b.chars().rev();
        loop {
            match (a_iter.next(), b_iter.next()) {
                (Some(a), Some(b)) => {
                    let sum = a.to_digit(10).unwrap() + b.to_digit(10).unwrap() + carry;
                    if sum == 0 {
                        s.push('0');
                        carry = 0;
                    } else if sum == 1 {
                        s.push('1');
                        carry = 0;
                    } else if sum == 2 {
                        s.push('0');
                        carry = 1;
                    } else {
                        s.push('1');
                        carry = 1;
                    }
                }
                (Some(a), None) => {
                    let sum = a.to_digit(10).unwrap() + carry;
                    if sum == 0 {
                        s.push('0');
                        carry = 0;
                    } else if sum == 1 {
                        s.push('1');
                        carry = 0;
                    } else {
                        s.push('0');
                        carry = 1;
                    }
                }
                (None, Some(a)) => {
                    let sum = a.to_digit(10).unwrap() + carry;
                    if sum == 0 {
                        s.push('0');
                        carry = 0;
                    } else if sum == 1 {
                        s.push('1');
                        carry = 0;
                    } else {
                        s.push('0');
                        carry = 1;
                    }
                }
                (None, None) => {
                    if carry > 0 {
                        s.push('1');
                    }
                    break;
                }
            }
        }
        s.iter().rev().collect()
    }
}

pub fn run() {
    let a = "11".to_string();
    let b = "1".to_string();
    assert_eq!(Solution::add_binary(a, b), "100".to_string());

    let a = "1010".to_string();
    let b = "1011".to_string();
    assert_eq!(Solution::add_binary(a, b), "10101".to_string());

    let a = "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111".to_string();
    let b = "1".to_string();
    Solution::add_binary(a, b);
}
